<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return view('email.booking');
    // return "Hello World, it\"s Lumen version ".$router->app->version();
});

//General Route
$router->get('tes', 'AuthController@tes');

$router->post('register-travel', 'AuthController@registerTravel');
$router->post('register-jamaah', 'AuthController@registerJamaah');

$router->post('login', 'AuthController@login');
$router->get('logout-dev', 'AuthController@logout');

$router->get('/email/verify', ['as' => 'email.verify', 'uses' => 'AuthController@emailVerify']);

$router->get('schedule/search', 'ScheduleController@search');
$router->get('schedule/detail/{id}', 'ScheduleController@show');

$router->post('activity-log/store', 'ActivityLogController@store');

$router->get('umr/img/{folder}/{name}', 'AssetsController@get');
$router->get('trip-type/get', 'TriptypeController@get');

$router->get('person-status/get', 'PersonstatusController@get');

$router->get('package/get', 'PackageController@get');

//All Roles Route
$router->group(['middleware' => ['auth', 'role:admin,travel,jamaah', 'verified']], function() use ($router) {
    $router->post('/email/request-verification', ['as' => 'email.request.verification', 'uses' => 'AuthController@emailRequestVerification']);
    $router->post('/password/reset-request', 'RequestPasswordController@sendResetLinkEmail');
    $router->post('change-password', 'AuthController@changePassword');
    $router->get('user/show/{id}', 'AuthController@showUser');
    $router->post('user/update', 'AuthController@updateUser');
    $router->get('logout', 'AuthController@logout');
    $router->get('mahram-fee/check/{id_mahram}/{age}/{id_travel}', 'MahramfeeController@check');
    $router->get('booking/detail/{id_booking}', 'BookingController@detail');
});

//Admin Route
$router->group(['middleware' => ['auth', 'role:admin']], function() use ($router) {
    $router->get('activity-log/get', 'ActivityLogController@get');
    $router->delete('activity-log/delete/{id}', 'ActivityLogController@delete');
    $router->get('travel/get', 'TravelController@get');

    $router->get('mahram/show/{id}', 'MahramController@show');
    $router->post('mahram/store', 'MahramController@store');
    $router->post('mahram/update', 'MahramController@update');
    $router->delete('mahram/delete/{id}', 'MahramController@delete');

    $router->get('mahram-fee/get', 'MahramfeeController@get');

    $router->post('trip-type/store', 'TriptypeController@store');
    $router->get('trip-type/show/{id}', 'TriptypeController@show');
    $router->post('trip-type/update', 'TriptypeController@update');
    $router->delete('trip-type/delete/{id}', 'TriptypeController@delete');

    $router->get('voucher/get', 'VoucherController@get');
});

//Travel Route
$router->group(['middleware' => ['auth', 'role:travel', 'verified']], function() use ($router) {
    $router->post('package/store', 'PackageController@store');
    $router->get('package/show/{id}', 'PackageController@show');
    $router->get('package/showByIdTravel/{id}', 'PackageController@showByIdTravel');
    $router->post('package/update', 'PackageController@update');
    $router->delete('package/delete/{id}', 'PackageController@delete');

    $router->post('travel/update', 'TravelController@update');
    $router->delete('travel/delete/{id}', 'TravelController@delete');

    $router->post('schedule/store', 'ScheduleController@store');
    $router->get('schedule/get', 'ScheduleController@get');
    $router->get('schedule/show/{id}', 'ScheduleController@show');
    $router->get('schedule/showByIdTravel/{id}', 'ScheduleController@showByIdTravel');
    $router->get('schedule/showByIdPackage/{id}', 'ScheduleController@showByIdPackage');
    $router->post('schedule/update', 'ScheduleController@update');
    $router->delete('schedule/delete/{id}', 'ScheduleController@delete');

    // $router->post('mahram-fee/store', 'MahramfeeController@store');
    $router->post('mahram-fee/update', 'MahramfeeController@update');
    $router->delete('mahram-fee/delete/{id}', 'MahramfeeController@delete');
});

//Jamaah Route
$router->group(['middleware' => ['auth', 'role:jamaah', 'verified']], function() use ($router) {
    $router->post('booking/store', 'BookingController@store');
    $router->get('booking/showByIdJamaah/{id_jamaah}', 'BookingController@showByIdJamaah');
});

//Admin & Travel Route
$router->group(['middleware' => ['auth', 'role:admin,travel', 'verified']], function() use ($router) {
    $router->get('travel/show/{id}', 'TravelController@show');

    $router->get('mahram/get/{id_travel}', 'MahramController@get');

    $router->get('mahram-fee/show/{id}/{id_mahram}', 'MahramfeeController@show');
    $router->get('mahram-fee/showByIdTravel/{id}', 'MahramfeeController@showByIdTravel');

    $router->post('voucher/store', 'VoucherController@store');
    $router->get('voucher/show/{id}', 'VoucherController@show');
    $router->get('voucher/showByIdTravel/{id}', 'VoucherController@showByIdTravel');
    $router->post('voucher/update', 'VoucherController@update');
    $router->delete('voucher/delete/{id}', 'VoucherController@delete');

});
