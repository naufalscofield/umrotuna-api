<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Mail;

use App\Mail\BookingEmail;

class SendBookingEmail extends Job
{
    protected $user;
    protected $details;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $details)
    {
        $this->user = $user;
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::to($this->user)->send(new BookingEmail($this->details));
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }
}