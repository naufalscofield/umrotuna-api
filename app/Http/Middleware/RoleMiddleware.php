<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        $user = Auth::user()->role;
        if($user) {
            foreach($roles as $role) {
                if($role == "admin" && $user == 1) {
                    return $next($request);
                }

                if($role == "travel" && $user == 2) {
                    return $next($request);
                }

                if($role == "jamaah" && $user == 3) {
                    return $next($request);
                }
            }
        } 
        
        return response()->json([
            'status' => 'Error',
            'code'   => 401,
            'message' => 'Permission denied!'
        ], 401);

    }
}
