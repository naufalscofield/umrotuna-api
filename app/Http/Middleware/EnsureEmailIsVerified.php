<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Auth;

class EnsureEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role == 1)
        {
            return $next($request);
        } else
        {
            if ( $request->fullUrl() != route('email.request.verification') && 
               ( ! $request->user() || ! $request->user()->hasVerifiedEmail() ) )
            {
                return response()->json([
                    'status' => 'Error',
                    'code'   => 403,
                    'message' => 'Unauthorized, your email address '.$request->user()->email.' is not verified.'
                ], 403);
                // throw new AuthorizationException('Unauthorized, your email address '.$request->user()->email.' is not verified.');
            }
            return $next($request);
        }
    }
}