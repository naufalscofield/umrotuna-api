<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Travel;
use App\Models\User;
use App\Models\Package;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\LogActivity;

class PackageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {

        $validator  = Validator::make($request->all(), [
            'id_travel'     => 'required',
            'package_name'  => 'required|string',
            'price'         => 'required|numeric',
            'dp'            => 'required|numeric',
            'currency'      => 'required|string',
            'description'   => 'required',
            'id_type'       => 'required',
            'refundable'    => 'required',
            'reschedule'    => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        if ($request->has('flyer'))
        {
            $flyer               = $request->file('flyer');
            $name                = $flyer->getClientOriginalName();
            $ext                 = $flyer->getClientOriginalExtension();
            $flyer_name          = 'PF-' . rand(000,999) . '-' . date('dmY') . '.' . $ext;
            $destination_path    = storage_path('/app/public/package_flyer');

            if ($request->file('flyer')->move($destination_path, $flyer_name)) {
                $package    = new Package;
                // return response()->json($package);
                $package->id_travel     = decrypt($request->id_travel);
                $package->id_type       = decrypt($request->id_type);
                $package->package_name  = $request->package_name;
                $package->price         = $request->price;
                $package->dp            = $request->dp;
                $package->currency      = $request->currency;
                $package->description   = $request->description;
                $package->flyer         = $flyer_name;
                $package->active        = 1;
                $package->refundable    = $request->refundable;
                $package->reschedule    = $request->reschedule;
        
                try {
                    $package->save();
                } catch (\Exception $e) {
                    unlink(storage_path('/app/public/package_flyer/'.$flyer_name));
                    return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot store new package, fail', 'error' => $e->getMessage()], 500);
                }
            } else
            {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot store package flyer image, fail'], 500);
            }
        } else
        {
            $package    = new Package;
            $package->id_travel     = decrypt($request->id_travel);
            $package->id_type       = decrypt($request->id_type);
            $package->package_name  = $request->package_name;
            $package->price         = $request->price;
            $package->dp            = $request->dp;
            $package->currency      = $request->currency;
            $package->description   = $request->description;
            $package->active        = 1;
            $package->refundable    = $request->refundable;
            $package->reschedule    = $request->reschedule;
            // return response()->json($package);

            try {
                $package->save();
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot store new package, fail', 'error' => $e->getMessage()], 500);
            }
        }

        LogActivity::addToLog($request, 'Create new trip package');
        return response()->json(['status' => 'success', 'code' => 200, 'data' => $package, 'message' => 'New package success added'], 200);
    }

    public function get(Request $request)
    {
        // dd(encrypt(1))
        $packages   = Package::with('triptype')->with('travel')->with('schedule')
                                ->get();

        LogActivity::addToLog($request, 'Get all trip package');

        return response()->json(['status' => 'success', 'code' => 200, 'data' => $packages], 200);
    }

    public function show(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $package   = Package::with('travel')
                                ->with('triptype')
                                ->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 404, 'message' => 'Data not found'], 404);
        }

        LogActivity::addToLog($request, 'Show a trip package');
        
        return response()->json(['status' => 'success', 'code' => 200, 'data' => $package], 200);
    }

    public function showByIdTravel(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $package   = Package::with('triptype')->where('id_travel', $id)->get();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 404, 'message' => 'Data not found'], 404);
        }
        
        LogActivity::addToLog($request, 'Show trip pacakges by travel');

        return response()->json(['status' => 'success', 'code' => 200, 'data' => $package], 200);
    }

    public function update(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'id_package'     => 'required',
            'id_type'        => 'required',
            'package_name'   => 'required',
            'price'          => 'required',
            'dp'             => 'required',
            'currency'       => 'required',
            'description'    => 'required',
            'active'         => 'required',
            'refundable'     => 'required',
            'reschedule'     => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        // return response()->json(decrypt($request->id_type));

        try {
            $package    = Package::findOrFail(decrypt($request->id_package));
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 404, 'message' => 'Data not found'], 404);
        }

        if ($request->has('flyer'))
        {
            $old_flyer           = decrypt($package->flyer);
            $flyer               = $request->file('flyer');
            $name                = $flyer->getClientOriginalName();
            $ext                 = $flyer->getClientOriginalExtension();
            $flyer_name          = 'PF-' . rand(000,999) . '-' . date('dmY') . '.' . $ext;
            $destination_path    = storage_path('/app/public/package_flyer');

            if ($request->file('flyer')->move($destination_path, $flyer_name)) {
                $package->package_name  = $request->package_name;
                $package->id_type       = decrypt($request->id_type);
                $package->price         = $request->price;
                $package->dp            = $request->dp;
                $package->currency      = $request->currency;
                $package->description   = $request->description;
                $package->flyer         = $flyer_name;
                $package->active        = $request->active;
                $package->refundable    = $request->refundable;
                $package->reschedule    = $request->reschedule;

                try {
                    $package->save();
                } catch (\Exception $e) {
                    unlink(storage_path('/app/public/package_flyer/'.$old_flyer));
                    return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()], 500);
                }
                unlink(storage_path('app/public/package_flyer/'.$old_flyer));
            } else {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot update flyer package, fail'], 500);
            }
        } else {
            $package->package_name  = $request->package_name;
            $package->id_type       = decrypt($request->id_type);
            $package->price         = $request->price;
            $package->dp            = $request->dp;
            $package->currency      = $request->currency;
            $package->description   = $request->description;
            $package->active        = $request->active;
            $package->refundable    = $request->refundable;
            $package->reschedule    = $request->reschedule;

            try {
                $package->save();
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot update package, fail'], 500);
            }
        }

        LogActivity::addToLog($request, 'Updating package');


        return response()->json(['status' => 'success', 'code' => 200, 'data' => $package, 'message' => 'Package updated'], 200);
    }

    public function delete(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $package   = Package::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 404, 'message' => 'Data not found'], 404);
        }

        try {
            $package->delete();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot delete package, fail'], 500);
        }

        LogActivity::addToLog($request, 'Delete a trip package');
        
        return response()->json(['status' => 'success', 'code' => 200, 'message' => 'Package deleted'], 200);
    }
}
