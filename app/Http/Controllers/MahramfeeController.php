<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use App\Models\Mahramfee;
use App\Models\Mahram;
use Illuminate\Support\Facades\Validator;

class MahramfeeController extends Controller
{
    public function check(Request $request, $id_status, $age, $id_travel)
    {
        try {
            $id_status = decrypt($id_status);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id status',
                'code' => 500
                ], 500);
        }

        try {
            $id_travel = decrypt($id_travel);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id travel',
                'code' => 500
                ], 500);
        }

        if (in_array($id_status, [1,7]) && $age < 17)
        {
            try {
                $check_mahram = Mahramfee::where(['id_travel' => $id_travel, 'id' => 1])->first();
            } catch (\Exception $e) {
                return response()->json([
                    'error' => $e->getMessage(),
                    'status' => 'error',
                    'message' => 'Failed to fetch data mahram fee',
                    'code' => 500
                ], 500);
            }
            $mahram_fee = ($check_mahram == NULL) ? NULL : $check_mahram->price;
        }

        if (in_array($id_status, [2,8]) && $age < 45)
        {
            try {
                $check_mahram = Mahramfee::where(['id_travel' => $id_travel, 'id' => 2])->first();
            } catch (\Exception $e) {
                return response()->json([
                    'error' => $e->getMessage(),
                    'status' => 'error',
                    'message' => 'Failed to fetch data mahram fee',
                    'code' => 500
                ], 500);
            }
            $mahram_fee = ($check_mahram == NULL) ? NULL : $check_mahram->price;
        }
            
        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data' => $mahram_fee
            ], 200);
        
        LogActivity::addToLog($request, 'Check mahram fee');
    }

    // public function store(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'id_travel'     => 'required',
    //         'id_mahram'     => 'required',
    //         'price'         => 'required',
    //     ]);

    //     if ($validator->fails())
    //     {
    //         $error_messages  = $validator->messages()->get('*');

    //         return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
    //     }

    //     $mahram = new Mahramfee;
    //     $mahram->id_travel    = decrypt($request->id_travel);
    //     $mahram->id_mahram    = decrypt($request->id_mahram);
    //     $mahram->price        = $request->price;
        
    //     try {
    //         $mahram->save();
    //     } catch (\Exception $e) {
    //         return response()->json([
    //             'status' => 'error',
    //             'message' => 'Failed to save data mahram fee',
    //             'error' => $e->getMessage(),
    //             'code' => 500,
    //             ], 500);
    //     }

    //     LogActivity::addToLog($request, 'Adding mahram fee');

    //     return response()->json([
    //         'status' => 'success',
    //         'code' => 200,
    //         'data'  => $mahram
    //         ], 200);
    // }

    public function get(Request $request)
    {
        try {
            $mahram = Mahramfee::with('mahram')
                                ->with('travel')
                                ->get();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data mahram fee',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Get mahram fee');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $mahram
            ], 200);
    }

    public function show(Request $request, $id, $id_mahram)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id mahram fee',
                'code' => 500
                ], 500);
        }

        try {
            $id_mahram = decrypt($id_mahram);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id mahram',
                'code' => 500
                ], 500);
        }

        try {
            $mahram = Mahramfee::with('travel')
                                ->with('mahram')
                                ->find($id);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data mahram fee',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        } 

        if ($mahram == null)
        {
            try {
            $mahram = Mahram::findOrFail($id_mahram);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Failed to fetch data mahram, not found',
                    'error' => $e->getMessage(),
                    'code' => 404,
                    ], 404);
            }
        }

        LogActivity::addToLog($request, 'Show mahram fee by id');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $mahram
            ], 200);
    }

    public function showByIdTravel(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id travel',
                'code' => 500
                ], 500);
        }

        try {
            $mahram = Mahramfee::with('travel')
                                ->with('mahram')
                                ->where('id_travel', $id)
                                ->get();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data mahram fee by travel, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        LogActivity::addToLog($request, 'Show mahram fee by travel');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $mahram
            ], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_travel'     => 'required',
            'id_mahram'     => 'required',
            'price'         => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        if (!$request->id_mahram_fee || $request->id_mahram_fee == NULL)
        {
            $mahram = new Mahramfee;
            $mahram->id_travel    = decrypt($request->id_travel);
            $mahram->id_mahram    = decrypt($request->id_mahram);
            $mahram->price        = $request->price;
            
            try {
                $mahram->save();
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Failed to save data mahram fee',
                    'error' => $e->getMessage(),
                    'code' => 500,
                    ], 500);
            }

            $msg = 'New mahram fee added';

            LogActivity::addToLog($request, 'Adding mahram fee');
        } else
        {
            try {
                $mahram = Mahramfee::findOrFail(decrypt($request->id_mahram_fee));
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Failed to fetch data mahram fee, data not found',
                    'error' => $e->getMessage(),
                    'code' => 404,
                    ], 404);
            }
    
            $mahram->id_mahram    = decrypt($request->id_mahram);
            $mahram->price        = $request->price;
            
            try {
                $mahram->save();
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Failed to update data mahram fee',
                    'error' => $e->getMessage(),
                    'code' => 500,
                    ], 500);
            }

            $msg = 'Mahram fee updated';
    
            LogActivity::addToLog($request, 'Update mahram fee');
        }


        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message' => $msg,
            'data'  => $mahram
            ], 200);
    }

    public function delete(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $mahram = Mahramfee::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data mahram fee, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        try {
            $mahram->delete();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to delete data mahram fee',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Delete mahram fee');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message'  => 'Mahram fee deleted'
            ], 200);
    }
}
