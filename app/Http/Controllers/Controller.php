<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    protected function respondWithToken($token, $user)
    {
        $user->enc_id_travel   = encrypt($user->id_travel);

        return response()->json([
            'code'  => 200,
            'token' => $token,
            'data'  => $user,   
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 15840
        ], 200);
    }
}
