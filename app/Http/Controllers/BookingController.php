<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Helpers\LogActivity;
use App\Models\Schedule;
use App\Models\Booking;
use App\Models\Bookingdetail;
use App\Models\Document;
use App\Models\Payment;
use App\Models\Mahramfee;
use App\Models\Voucher;
use App\Models\User;
use Carbon\Carbon;
use App\Jobs\SendBookingEmail;

class BookingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_user'          => 'required',
            'id_schedule'      => 'required',
            'name[*]'          => 'required',
            'nik[*]'           => 'required|digits:16|unique:booking_detail',
            'age_at_departure[*]' => 'required|numeric',
            'birthdate[*]'     => 'required',
            'gender[*]'        => 'required',
            'status[*]'        => 'required',
            'kk[*]'            => 'required',
        ]);
        
        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');
            
            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        try {
            $request->id_user       = decrypt($request->id_user);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Decryption issue on id user', 'error' => $e->getMessage(), 'code' => 500], 500);
        }

        try {
            $request->id_schedule   = decrypt($request->id_schedule);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Decryption issue on id user', 'error' => $e->getMessage(), 'code' => 500], 500);
        }

        try {
            $user = User::findOrFail($request->id_user);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'User not found', 'error' => $e->getMessage(), 'code' => 404], 404);
        }


        $check_book             = Booking::whereDate('created_at', Carbon::today())->get()->count();

        try {
            $schedule           = Schedule::with('package')->with('package.travel')->findOrFail($request->id_schedule);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => 'error',
                'message'   => 'Schedule not found',
                'error'     => $e->getMessage(),
                'code'      => 404
            ],404);
        }

        if ($schedule->quota == 0 || $schedule->status == 0)
        {
            return response()->json([
                'status'    => 'warning',
                'message'   => 'The quota for this schedule is full',
                'code'      => 400
            ],400);
        }

        //Currently support only rupiah currency
        $package_price          = $schedule->package->price;                
        $total_person           = count($request->name);

        //Insert table Booking
        $booking                = new Booking;
        $booking->id_user       = $request->id_user;
        $booking->id_schedule   = $request->id_schedule;
        $booking->booking_code  = 'UMRB'.date('Ymd').str_pad($check_book+1, 4, "0", STR_PAD_LEFT);;
        $booking->status        = 1;

        DB::beginTransaction();

        try {
            $booking->save();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error',  'message' => 'Cannot save data booking', 'error' => $e->getMessage(), 'code' => 500], 500);
        }

        $total_mahram   = 0;
        $arr_ktp        = [];
        $arr_kk         = [];

        foreach ($request->name as $idx => $rn)
        {
            // if (!in_array(decrypt($request->status[$idx]), [5,6,7,8,10]) && $request->file('ktp')[$idx] == false)
            // {
            //     return response()->json(['status' => 'error',  'message' => 'KTP is required for adult person', 'code' => 400], 400);
            // }

            // Insert Booking Detail
            $detail      = new Bookingdetail;

            $detail->id_booking   = $booking->id;
            $detail->name         = $rn;
            $detail->nik          = $request->nik[$idx];
            $detail->birthdate    = $request->birthdate[$idx];
            $detail->age_at_departure          = $request->age_at_departure[$idx];
            $detail->gender       = $request->gender[$idx];
            $detail->status       = decrypt($request->status[$idx]);

            if ($total_person == 1 && in_array(decrypt($request->status[$idx]), [1,7]) && $request->age_at_departure[$idx] < 17)
            {
                $check_mahram = Mahramfee::where(['id_travel' => $schedule->package->travel->id, 'id' => 1])->first();

                $detail->mahram_fee = ($check_mahram == NULL) ? NULL : $check_mahram->price;
            }

            if ($total_person == 1 && in_array(decrypt($request->status[$idx]), [2,8]) && $request->age_at_departure[$idx] < 45)
            {
                $check_mahram = Mahramfee::where(['id_travel' => $schedule->package->travel->id, 'id' => 2])->first();

                $detail->mahram_fee = ($check_mahram == NULL) ? NULL : $check_mahram->price;
            }

            try {
                $detail->save();
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Cannot save detail booking, fail',
                    'error'     => $e->getMessage(),
                    'code'      => 500
                ],500);
            }
            
            $docs                       = new Document;
            $docs->id_booking_detail    = $detail->id;

            //KTP
            if (Carbon::parse($request->birthdate[$idx])->age >= 17)
            {
                if ($request->file('ktp')[$idx])
                {
                    $ktp               = $request->file('ktp')[$idx];
                    $name              = $ktp->getClientOriginalName();
                    $ext               = $ktp->getClientOriginalExtension();
                    $ktp_name          = 'KTP-' . rand(000,999) . '-' . date('dmY') . '.' . $ext;
                    $destination_path  = storage_path('/app/public/ktp/');
        
                    if ($request->file('ktp')[$idx]->move($destination_path, $ktp_name))
                    {
                        array_push($arr_ktp, $ktp_name);
                    } else 
                    {
                        DB::rollback();
                        return response()->json([
                            'status'    => 'error',
                            'message'   => 'Cannot upload ktp, fail',
                            'error'     => $e->getMessage(),
                            'code'      => 500
                        ],500);
                    }
                    $docs->ktp                  = $ktp_name;

                } else
                {
                    DB::rollback();
                    return response()->json([
                        'status'    => 'error',
                        'message'   => $rn.' : Person who are greater than 17 years old today, required a KTP',
                        'code'      => 400
                    ],400);
                }
            }

            //KK
            $kk               = $request->file('kk')[$idx];
            $name             = $kk->getClientOriginalName();
            $ext              = $kk->getClientOriginalExtension();
            $kk_name          = 'KK-' . rand(000,999) . '-' . date('dmY') . '.' . $ext;
            $destination_path = storage_path('/app/public/kk/');

            if ($request->file('kk')[$idx]->move($destination_path, $kk_name))
            {
                array_push($arr_kk, $kk_name);
            } else
            {
                unlink(storage_path('/app/public/ktp/'.$ktp_name));
                DB::rollback();
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Cannot upload kk, fail',
                    'error'     => $e->getMessage(),
                    'code'      => 500
                ],500);
            }
            $docs->kk                   = $kk_name;

            try {
                $docs->save();
            } catch (\Exception $e)
            {
                if ($request->file('ktp')[$idx])
                {
                unlink(storage_path('/app/public/ktp/'.$ktp_name));
                }
                unlink(storage_path('/app/public/kk/'.$kk_name));
                DB::rollback();
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Cannot save data document, fail',
                    'error'     => $e->getMessage(),
                    'code'      => 500
                ],500);
            }
        }

        $total_payment          = ($package_price * $total_person) + $total_mahram;
        $total_dp               = $schedule->package->dp * $total_person;

        //Insert Payment Table For DP
        $payment                = new Payment;
        $payment->id_booking    = $booking->id;
        $payment->type          = 'dp';
        $payment->total_payment = $total_dp;
        $payment->expired       = Carbon::now()->addDays(3)->toDateString();

        try {
            $payment->save();
        } catch (\Exception $e) {
            foreach ($arr_ktp as $akt)
                {
                    unlink(storage_path('/app/public/ktp/'.$akt));
                }
                foreach ($arr_kk as $ak)
                {
                    unlink(storage_path('/app/public/kk/'.$ak));
                }
                DB::rollback();
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Failed to save payment data DP',
                    'error'     => $e->getMessage(),
                    'code'      => 500
                ],500);
        }
        //

        //Insert Payment Table For Repayment
        $payment                = new Payment;
        $payment->id_booking    = $booking->id;
        $payment->type          = 'repayment';
        $payment->expired       = Carbon::now()->subDays(14)->toDateString();

        if ($request->has('id_voucher'))
        {
            try {
                $voucher              = Voucher::findOrFail(decrypt($request->id_voucher));
            } catch (\Exception $e) {
                foreach ($arr_ktp as $akt)
                {
                    unlink(storage_path('/app/public/ktp/'.$akt));
                }
                foreach ($arr_kk as $ak)
                {
                    unlink(storage_path('/app/public/kk/'.$ak));
                }
                DB::rollback();
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Voucher not found',
                    'error'     => $e->getMessage(),
                    'code'      => 404
                ],404);
            }

            $payment->id_voucher      = $request->id_voucher;

            if ($voucher->type == 'percentage')
            {
                $cut                    = ($voucher->discount / 100) * $total_payment;
                $payment->total_payment = ($total_payment - $total_dp) - $cut;
                $payment->total_discount= $cut;
            } else if ($voucher->type == 'nominal')
            {
                $payment->total_payment = ($total_payment - $total_dp) - $voucher->discount;
                $payment->total_discount= $voucher->discount;
            }
        } else
        {
            $payment->total_payment = $total_payment - $total_dp;
        }

        try {
            $payment->save();
        } catch (\Exception $e) {
            foreach ($arr_ktp as $akt)
            {
                unlink(storage_path('/app/public/ktp/'.$akt));
            }
            foreach ($arr_kk as $ak)
            {
                unlink(storage_path('/app/public/kk/'.$ak));
            }
            DB::rollback();
            return response()->json([
                'status'    => 'error',
                'message'   => 'Cannot save data payment, fail',
                'error'     => $e->getMessage(),
                'code'      => 500
            ],500);
        }

        $schedule->quota    = $schedule->quota - $total_person;

        try {
            $schedule->save();
        } catch (\Exception $e) {
            foreach ($arr_ktp as $akt)
            {
                unlink(storage_path('/app/public/ktp/'.$akt));
            }
            foreach ($arr_kk as $ak)
            {
                unlink(storage_path('/app/public/kk/'.$ak));
            }
            DB::rollback();
            return response()->json([
                'status'    => 'error',
                'message'   => 'Cannot update quota, fail',
                'error'     => $e->getMessage(),
                'code'      => 500
            ],500);
        }

        DB::commit();

        LogActivity::addToLog($request, 'Booking a schedule');


        // Send Email
        $booking_detail     = Bookingdetail::select('name','gender')->where('id_booking', $booking->id)->get();

        $data = [
            'name'  => $user->first_name,
            'booking_code' => $booking->code,
            'travel_detail' => $schedule->package->travel->travel_name.' - '.$schedule->package->package_name,
            'departure_detail'  => Carbon::parse($schedule->departure_date)->format('d F Y').' from '.$schedule->city,
            'persons'   => $booking_detail,
            'expired_dp'    => Carbon::now()->addDays(3)->format('d F Y')
        ];

        dispatch(new SendBookingEmail($user->email, $data));
        //

        $return_payment = Payment::where('id_booking', $booking->id)->get();

        return response()->json([
            'status'    => 'success',
            'message'   => 'Booking success',
            'code'      => 200,
            'data'      => [
                'booking'   => $booking
            ]
        ],200);
    }

    public function detail(Request $request, $id_booking)
    {
        try {
            $id_booking = decrypt($id_booking);
        }
        catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Decryption issue on id booking', 'error' => $e->getMessage(), 'code' => 500], 500);
        }
        
        try {
            $booking = Booking::with('schedule')->with('schedule.package')->with('schedule.package.travel')->findOrFail($id_booking);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Fail to get data booking, data may not found', 'error' => $e->getMessage(), 'code' => 404], 404);
        }
        
        try {
            $detail = Bookingdetail::with('document')->with('status')->where('id_booking', $id_booking)->get();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Fail to get data detail booking', 'error' => $e->getMessage(), 'code' => 500], 500);
        }
        
        try {
            $payment = Payment::where('id_booking', $id_booking)->get();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Fail to get data payment booking', 'error' => $e->getMessage(), 'code' => 500], 500);
        }

        $total_payment  = 0;
        $total_dp       = 0;
        $total_repayment= 0;
        $total_discount = 0;
        $total_mahram   = 0;

        foreach ($payment as $p) {
            $total_payment  += $p->total_payment;

            // return response()->json($p->type);
            if ($p->type == 'dp')
            {
                $total_dp += $p->total_payment;
            }

            if ($p->type == 'repayment')
            {
                $total_repayment += $p->total_payment;
            }

            if ($p->total_discount != NULL)
            {
                $total_discount += $p->total_discount;
            }
        }

        foreach ($detail as $d) {
            $total_mahram   += $d->mahram_fee;
        }

        $data = [
        'total_payment_before_discount' => $total_payment+$total_discount,  
        'total_payment_after_discount' => $total_payment,  
        'total_dp' => $total_dp,       
        'total_repayment' => $total_repayment,
        'total_discount' => $total_discount, 
        'total_mahram' => $total_mahram,
        'booking_data'    => $booking,
        'booking_detail'    => $detail
        ]; 

        LogActivity::addToLog($request, 'Get booking detail');

        return response()->json(['status' => 'success', 'data' => $data, 'code' => 200], 200);
    }

    public function showByIdJamaah(request $request, $id_jamaah)
    {
        try {
            $id_jamaah  = decrypt($id_jamaah);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Decryption issue on id jamaah', 'error' => $e->getMessage(), 'code' => 500], 500);
        }

        $booking   = Booking::with('schedule')
                            ->with('schedule.package')
                            ->with('schedule.package.travel')
                            ->with('payment')
                            ->withCount('persons')
                            ->where('id_user', $id_jamaah);

        $booking->withCount([
            'payment AS sub_total_payment' => function ($query) {
                        $query->select(DB::raw("SUM(total_payment) as paidsum"));
                    }
                ]);

        if ($request->input('status') && $request->input('status') != NULL)
        {
            // if (in_array($request->input('status'), ['waiting', 'process', 'decline', 'done', 'expired']))
            // {
            //     switch($request->input('status')) {
            //         case('waiting'):
            //             $status = 1;
            //             break;
            //         case('process'):
            //             $status = 2;
            //             break;
            //         case('decline'):
            //             $status = 3;
            //             break;
            //         case('done'):
            //             $status = 4;
            //             break;
            //         case('expired'):
            //             $status = 5;
            //             break;
            //     }
                $booking->where('status', $request->input('status'));
            // } 
        }

        $data = $booking->get();

        return response()->json(['status' => 'success', 'data' => $data, 'code' => 200], 200);
    }
}
