<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use App\Models\Mahram;
use Illuminate\Support\Facades\Validator;
use DB;

class MahramController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description'   => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        $mahram = new Mahram;
        $mahram->description    = $request->description;
        
        try {
            $mahram->save();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to save data mahram',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Adding mahram');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $mahram
            ], 200);
    }

    public function get(Request $request, $id_travel = NULL)
    {
        if ($id_travel != NULL)
        {
            try {
                $id_travel  = decrypt($id_travel);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Decryption issue on id travel',
                    'error' => $e->getMessage(),
                    'code' => 500,
                    ], 500);
            }
            try {
                $mahram         = \DB::table('mahram')
                                    ->select('mahram.id as id_mahram', 'mahram.description', 'mahram_fee.id as id_mahram_fee', 'mahram_fee.price as price')
                                    ->leftJoin('mahram_fee',function ($join) use($id_travel) {
                                        $join->on('mahram_fee.id_mahram', '=' , 'mahram.id') ;
                                        $join->where('mahram_fee.id_travel','=',$id_travel) ;
                                    })
                                    ->get();
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Failed to fetch data mahram',
                    'error' => $e->getMessage(),
                    'code' => 500,
                ], 500);
            }
            foreach ($mahram as $m)
            {
                $m->enc_id = encrypt($m->id_mahram);
                $m->enc_id_mahram_fee = encrypt($m->id_mahram_fee);
            }
        } else
        {
            try {
                $mahram = Mahram::all();
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Failed to fetch data mahram',
                    'error' => $e->getMessage(),
                    'code' => 500,
                    ], 500);
            }
        }

        LogActivity::addToLog($request, 'Get mahram');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $mahram
            ], 200);
    }

    public function show(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id mahram',
                'code' => 500
                ], 500);
        }

        try {
            $mahram = Mahram::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data mahram, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        LogActivity::addToLog($request, 'Show mahram by id');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $mahram
            ], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_mahram'     => 'required',
            'description'   => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        try {
            $mahram = Mahram::findOrFail(decrypt($request->id_mahram));
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data mahram, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        $mahram->description    = $request->description;
        
        try {
            $mahram->save();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to update data mahram',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Update mahram');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $mahram
            ], 200);
    }

    public function delete(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id mahram',
                'code' => 500
                ], 500);
        }

        try {
            $mahram = Mahram::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data mahram, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        try {
            $mahram->delete();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to delete data mahram',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Delete mahram');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message'  => 'Mahram deleted'
            ], 200);
    }
}
