<?php

namespace App\Http\Controllers;

use App\Helpers\LogActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ActivityLogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subject'       => 'required'
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        try 
        {
           LogActivity::addToLog($request, $request->subject);
        } catch (\Exception $e)
        {
           return response()->json(['message' => $e->getMessage(), 'status' => 'error', 'code' => 500], 500);
        }

        return response()->json(['message' => 'Log saved', 'status' => 'success', 'code' => 200], 200);
    }

    public function get()
    {
        try 
        {
            $logs = LogActivity::logActivityLists();
        } catch (\Exception $e)
        {
           return response()->json(['message' => $e->getMessage(), 'status' => 'error', 'code' => 500], 500);
        }

        return response()->json(['data' => $logs, 'status' => 'success', 'code' => 200], 200);
    }

    public function delete($id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try 
        {
           LogActivity::deleteLog($id);
        } catch (\Exception $e)
        {
           return response()->json(['message' => $e->getMessage(), 'status' => 'error', 'code' => 500], 500);
        }

        return response()->json(['message' => 'Log deleted', 'status' => 'success', 'code' => 200], 200);
    }

    //
}
