<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Travel;
use App\Models\User;
use App\Models\Package;
use App\Models\Schedule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\LogActivity;

class ScheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'id_package'     => 'required',
            'city'           => 'required',
            'departure_date' => 'required',
            'return_date'    => 'required',
            'description'    => 'required',
            'quota'          => 'required',
            'active'         => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        if ($request->return_date < $request->departure_date)
        {
            return response()->json(['status' => 'error', 'code' => 400, 'message' => 'Return date cannot before departure date'], 400);
        }

        if ($request->has('flyer'))
        {
            $flyer               = $request->file('flyer');
            $name                = $flyer->getClientOriginalName();
            $ext                 = $flyer->getClientOriginalExtension();
            $flyer_name          = 'F-' . rand(000,999) . '-' . date('dmY') . '.' . $ext;
            $destination_path    = storage_path('/app/public/schedule_flyer');

            if ($request->file('flyer')->move($destination_path, $flyer_name)) {
                $schedule                = new Schedule;
                $schedule->id_package    = $request->id_package;
                $schedule->city          = $request->city;
                $schedule->departure_date= $request->departure_date;
                $schedule->return_date   = $request->return_date;
                $schedule->description   = $request->description;
                $schedule->quota         = $request->quota;
                $schedule->status        = 1;
                $schedule->flyer         = $flyer_name;
                $schedule->active        = $request->active;

                try {
                    $schedule->save();
                } catch (\Exception $e) {
                    unlink(storage_path('/app/public/schedule_flyer/'.$flyer_name));
                    return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()], 500);
                }
            } else {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot save flyer schedule, fail'], 500);
            }
        } else {
            $schedule                = new Schedule;
            $schedule->id_package    = decrypt($request->id_package);
            $schedule->city          = $request->city;
            $schedule->departure_date= $request->departure_date;
            $schedule->return_date   = $request->return_date;
            $schedule->description   = $request->description;
            $schedule->quota         = $request->quota;
            $schedule->status        = 1;
            $schedule->active        = $request->active;

            try {
                $schedule->save();
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()], 500);
            }
        }

        LogActivity::addToLog($request, 'Create new schedule');

        return response()->json(['status' => 'success', 'code' => 200, 'data' => $schedule, 'message' => 'New schedule success added'], 200);
    }   

    public function get(Request $request)
    {
        $schedule   = Schedule::with('package')
                                ->with('package.travel')
                                ->get();

        LogActivity::addToLog($request, 'Get all schedule');

        return response()->json(['status' => 'success', 'code' => 200, 'data' => $schedule], 200);
    }

    public function show(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $schedule   = Schedule::with('package')->with('package.travel')->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 404, 'message' => 'Data not found'], 404);
        }

        LogActivity::addToLog($request, 'Show a schedule');
        
        return response()->json(['status' => 'success', 'code' => 200, 'data' => $schedule], 200);
    }

    public function showByIdTravel(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $schedule   = Schedule::with('package')
                                ->where('package.id_travel', $id)
                                ->get();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()], 500);
        }

        LogActivity::addToLog($request, 'Show a schedule by travel');

        return response()->json(['status' => 'success', 'code' => 200, 'data' => $schedule], 200);
    }

    public function showByIdPackage(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $schedule   = Schedule::where('id_package', $id)
                                ->get();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()], 500);
        }
        
        LogActivity::addToLog($request, 'Show a schedule by package');

        return response()->json(['status' => 'success', 'code' => 200, 'data' => $schedule], 200);
    }

    public function search(Request $request)
    {
        $schedule   = Schedule::with('package')
                                ->with('package.travel')
                                ->where('active', 1);

        if ($request->input('id_package') && $request->input('id_package') != NULL)
        {
            $schedule->where('id_package', decrypt($request->input('id_package')));
        }

        if ($request->input('city') && $request->input('city') != NULL)
        {
            $schedule->where('city', $request->input('city'));
        }
        
        if ($request->input('periodefrom') && $request->input('periodefrom') != NULL)
        {
            $schedule->where('departure_date', '>=', $request->input('periodefrom'));
        }
        
        if ($request->input('periodeto') && $request->input('periodeto') != NULL)
        {
            $schedule->where('departure_date', '<=', $request->input('periodeto'));
        }
        
        if ($request->input('orderperiode') && $request->input('orderperiode') != NULL && in_array($request->input('orderperiode'), ['ASC', 'asc', 'DESC', 'desc']))
        {
            $schedule->orderBy('departure_date', strtoupper($request->input('orderperiode')));
        }
        
        $schedule->whereHas('package', function ($query) use($request) {
            if ($request->input('type') && $request->input('type') != NULL)
            {
                $query->where('id_type', $request->input('type'));
            }

            if ($request->input('minprice') && $request->input('minprice') != NULL)
            {
                $query->where('price', '>=', $request->input('minprice'));
            }
        
            if ($request->input('maxprice') && $request->input('maxprice') != NULL)
            {
                $query->where('price', '<=', $request->input('maxprice'));
            }

            if ($request->input('orderprice') && $request->input('orderprice') != NULL && in_array($request->input('orderprice'), ['ASC', 'asc', 'DESC', 'desc']))
            {
                $schedule->orderBy('price', strtoupper($request->input('orderprice')));
            }
        });

        try {
            $data = $schedule->get();
        } catch (\Exception $e)
        {
            return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()]);
        }

        LogActivity::addToLog($request, 'Show a schedule by user request');
        
        return response()->json(['status' => 'success', 'code' => 200, 'data' => $data]);
    }

    public function update(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'id_schedule'    => 'required',
            'id_package'     => 'required',
            'city'           => 'required',
            'departure_date' => 'required',
            'return_date'    => 'required',
            'description'    => 'required',
            'quota'          => 'required',
            'status'         => 'required',
            'active'         => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        try {
            $schedule    = Schedule::findOrFail(decrypt($request->id_schedule));
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Decryption issue on id schedule'], 500);
        }
        
        if ($request->return_date < $request->departure_date)
        {
            return response()->json(['status' => 'error', 'code' => 400, 'message' => 'Return date cannot before departure date'], 400);
        }

        if ($request->has('flyer'))
        {
            $old_flyer           = decrypt($schedule->flyer);
            $flyer               = $request->file('flyer');
            $name                = $flyer->getClientOriginalName();
            $ext                 = $flyer->getClientOriginalExtension();
            $flyer_name          = 'F-' . rand(000,999) . '-' . date('dmY') . '.' . $ext;
            $destination_path    = storage_path('/app/public/schedule_flyer');

            if ($request->file('flyer')->move($destination_path, $flyer_name)) {
                $schedule->id_package    = decrypt($request->id_package);
                $schedule->city          = $request->city;
                $schedule->departure_date= $request->departure_date;
                $schedule->return_date   = $request->return_date;
                $schedule->description   = $request->description;
                $schedule->quota         = $request->quota;
                $schedule->status        = $request->status;
                $schedule->flyer         = $flyer_name;
                $schedule->active        = $request->active;

                try {
                    $schedule->save();
                } catch (\Exception $e) {
                    unlink(storage_path('/app/public/schedule_flyer/'.$old_flyer));
                    return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()], 500);
                }
                unlink(storage_path('/app/public/schedule_flyer/'.$old_flyer));
            } else {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot update flyer schedule, fail'], 500);
            }
        } else {
            $schedule->id_package    = decrypt($request->id_package);
            $schedule->city          = $request->city;
            $schedule->departure_date= $request->departure_date;
            $schedule->return_date   = $request->return_date;
            $schedule->description   = $request->description;
            $schedule->quota         = $request->quota;
            $schedule->status        = $request->status;
            $schedule->active        = $request->active;

            try {
                $schedule->save();
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot update schedule, fail'], 500);
            }
        }

        LogActivity::addToLog($request, 'Updating schedule');


        return response()->json(['status' => 'success', 'code' => 200, 'data' => $schedule, 'message' => 'Schedule updated'], 200);
    }

    public function delete(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $schedule   = Schedule::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 404, 'message' => 'Data not found'], 404);
        }

        try {
            $schedule->delete();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot delete schedule, fail'], 500);
        }

        LogActivity::addToLog($request, 'Delete a schedule');
        
        return response()->json(['status' => 'success', 'code' => 200, 'message' => 'Schedule deleted'], 200);
    }
}
