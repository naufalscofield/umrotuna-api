<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Http\Request;


class AssetsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get($folder, $name)
    {
        switch($folder) {
            case('tl'):
                $f  = 'travel_logo';
                break;
            case('pf'):
                $f  = 'package_flyer';
                break;
            case('sf'):
                $f  = 'schedule_flyer';
                break;
            case('ic'):
                $f  = 'ktp';
                break;
            case('fc'):
                $f  = 'kk';
                break;
            case('pp'):
                $f  = 'passport';
                break;
            case('mb'):
                $f  = 'marriage_book';
            case('bc'):
                $f  = 'birth_certificate';
            case('vm'):
                $f  = 'vaccine_meningitis';
            case('pt'):
                $f  = 'photo';
                break;
        }

        $n = decrypt($name);

        $type       = 'image/png';
        $headers    = ['Content-Type' => $type];
        $path       = storage_path('/app/public/'.$f.'/'.$n);

        $response = new BinaryFileResponse($path, 200 , $headers);

        return $response;
    }

    //
}
