<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use App\Models\Triptype;
use Illuminate\Support\Facades\Validator;

class TriptypeController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type'          => 'required',
            'description'   => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        $type = new Triptype;
        $type->type          = $request->type;
        $type->description   = $request->description;
        
        try {
            $type->save();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to save data trip type',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Adding trip type');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $type
            ], 200);
    }

    public function get(Request $request)
    {
        try {
            $type = Triptype::all();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data trip type',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Get trip type');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $type
            ], 200);
    }

    public function show(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id trip type',
                'code' => 500
                ], 500);
        }

        try {
            $type = Triptype::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data trip type, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        LogActivity::addToLog($request, 'Show trip type by id');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $type
            ], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_trip_type'   => 'required',
            'type'           => 'required',
            'description'    => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        try {
            $request->id_trip_type = decrypt($request->id_trip_type);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Decryption issue on id trip type',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        try {
            $type = Triptype::findOrFail($request->id_trip_type);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data trip type, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        $type->description    = $request->description;
        $type->type           = $request->type;
        
        try {
            $type->save();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to update data trip type',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Update trip type');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $type
            ], 200);
    }

    public function delete(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id trip typr',
                'code' => 500
                ], 500);
        }

        try {
            $type = Triptype::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data trip type, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        try {
            $type->delete();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to delete data trip type',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Delete trip type');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message'  => 'Trip type deleted'
            ], 200);
    }
}
