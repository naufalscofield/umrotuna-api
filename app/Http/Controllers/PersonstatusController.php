<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use App\Models\Personstatus;
use Illuminate\Support\Facades\Validator;

class PersonstatusController extends Controller
{
    public function get(Request $request)
    {
        try {
            $person = Personstatus::all();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data person status',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Get person status');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $person
            ], 200);
    }
}
