<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Travel;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Helpers\LogActivity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendBookingEmail;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function registerTravel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'travel_name'   => 'required',
            'email'         => 'required',
            'address'       => 'required',
            'phone'         => 'required|min:11|max:13',
            'logo'          => 'required'
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        DB::beginTransaction();

        $logo               = $request->file('logo');
        $name               = $logo->getClientOriginalName();
        $ext                = $logo->getClientOriginalExtension();
        $logo_name          = 'T-' . rand(000,999) . '-' . date('dmY') . '.' . $ext;
        $destination_path   = storage_path('/app/public/travel_logo/');

        if ($request->file('logo')->move($destination_path, $logo_name)) {
            $travel = new Travel;
        
            $travel->travel_name    = $request->travel_name;
            $travel->email          = $request->email;
            $travel->address        = $request->address;
            $travel->phone          = $request->phone;
            $travel->logo           = $logo_name;
            $travel->social_media_1 = $request->social_media_1;
            $travel->social_media_type_1 = $request->social_media_type_1;
            $travel->social_media_2 = $request->social_media_2;
            $travel->social_media_type_2 = $request->social_media_type_2;
            $travel->website        = $request->website;
            $travel->active         = 0;

            try {
                $travel->save();
            } catch (\Exception $e) {
                unlink(storage_path('/app/public/travel_logo/'.$logo_name));
                return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()], 500);
            }

            LogActivity::addToLog($request, 'Register new travel');

            $user   = new User;
            
            $user->email        = $request->email;
            $user->password     = Hash::make($request->email);
            $user->birthdate    = NULL;
            $user->first_name   = 'Admin';
            $user->last_name    = $request->travel_name;
            $user->address      = NULL;
            $user->phone        = NULL;
            $user->role         = 2;
            $user->id_travel    = $travel->id;

            try {
                $user->save();
            } catch (\Exception $e) {
                DB::rollback();
                unlink(storage_path('/app/public/travel_logo/'.$logo_name));
                return response()->json(['status' => 'error', 'code' => 500, 'message' => $e->getMessage()], 500);
            }

            LogActivity::addToLog($request, 'Register new admin travel');
            
            DB::commit();
            $res    = [
                'travel'        => $travel,
                'admin_travel'  => $user
            ];

            return response()->json(['data' => $res, 'status' => 'success', 'code' => 200, 'message' => 'New travel added'], 200);
        } else {
            return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot upload file'], 500);
        }

    }

    public function registerJamaah(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'         => 'required',
            'password'      => 'required',
            'birthdate'     => 'required',
            'gender'        => 'required|max:1',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'address'       => 'required',
            'phone'         => 'required|min:11|max:13',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        $check  = User::where('email', $request->email)->first();

        if ($check != NULL)
        {
            return response()->json(['status' => 'error', 'code' => 403, 'message' => 'email already registered'], 403);
        } else
        {
            $user   = new User;
                
            $user->email        = $request->email;
            $user->password     = Hash::make($request->password);
            $user->birthdate    = $request->birthdate;
            $user->gender       = $request->gender;
            $user->first_name   = $request->first_name;
            $user->last_name    = $request->last_name;
            $user->address      = $request->address;
            $user->phone        = $request->phone;
            $user->role         = 3;
    
            try {
                $user->save();
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot save new user jamaah, fail'], 500);
            }

            LogActivity::addToLog($request, 'Register new user jamaah');

            return response()->json(['data' => $user, 'status' => 'success', 'code' => 200, 'message' => 'New user jamaah added'], 200);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'         => 'required',
            'password'      => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }
        
        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['status' => 'error', 'code' => 401, 'message' => 'Invalid credentials'], 401);
        }

        $user  = Auth::user();

        LogActivity::addToLog($request, 'Login');

        return $this->respondWithToken($token, $user);
    }

    public function logout(Request $request)
    {
        try {
            auth()->logout();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'code' => 500], 500);
        }
        
        LogActivity::addToLog($request, 'Logout');
        return response()->json(['status' => 'success', 'message' => 'User successfully signed out', 'code' => 200], 200);
    }

    public function emailRequestVerification(Request $request)
    {
        if ( $request->user()->hasVerifiedEmail() ) {
            return response()->json('Email address is already verified.');
        }
        
        $request->user()->sendEmailVerificationNotification();
        
        LogActivity::addToLog($request, 'Request verification email');

        return response()->json('Email request verification sent to '. Auth::user()->email);
    }

    public function emailVerify(Request $request)
    {
        $this->validate($request, [
        'token' => 'required|string',
        ]);

        \Tymon\JWTAuth\Facades\JWTAuth::getToken();
            \Tymon\JWTAuth\Facades\JWTAuth::parseToken()->authenticate();
        if ( ! $request->user() ) {
            return redirect('https://tijari.online/umroh');
            // return response()->json('Invalid token', 401);
        }
            
        if ( $request->user()->hasVerifiedEmail() ) {
            return redirect('https://tijari.online/umroh');
            // return response()->json('Email address '.$request->user()->getEmailForVerification().' is already verified.');
        }

        $request->user()->markEmailAsVerified();
        LogActivity::addToLog($request, 'Verifying email');
        return redirect('https://tijari.online/umroh');
        // return response()->json('Email address '. $request->user()->email.' successfully verified.');
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_user'       => 'required',
            'old_password'  => 'required',
            'new_password'  => 'required',
        ]);

        $user   = User::findOrFail(decrypt($request->id_user));

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        if (Hash::check($request->old_password, $user->password)) { 
            $user->password = Hash::make($request->new_password);

            try {
                $user->save();
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => "Cannot change password, fail"], 500);
            }
            LogActivity::addToLog($request, 'Changing password');
            return response()->json(['status' => 'success', 'code' => 200, 'message' => "Change password success"], 200);
        } else {
            return response()->json(['status' => 'error', 'code' => 400, 'message' => "Old password doesn't match"], 400);
        }
        
    }

    public function showUser(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }
        
        try {
            $user   = User::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 404, 'message' => 'User not found'], 404);
        }
        
        if ($user->role == 2)
        {
            try {
                $travel = Travel::findOrFail($user->id_travel);
            } catch (\Exception $e)
            {
                return response()->json(['status' => 'success', 'code' => 200, 'data' => $user], 200);
            }
            
            $user['travel'] = $travel;
            
            return response()->json(['status' => 'success', 'code' => 200, 'data' => $user], 200);
        } else
        {
            return response()->json(['status' => 'success', 'code' => 200, 'data' => $user], 200);
        }

        LogActivity::addToLog($request, 'Show user by id');
    }

    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_user'       => 'required',
            'birthdate'     => 'required',
            'gender'        => 'required|digits:1',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'address'       => 'required',
            'phone'         => 'required|min:11|max:13',
        ]);

        if($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }
        
        try {
            $user   = User::findOrFail(decrypt($request->id_user));
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 404, 'message' => 'User not found'], 404);
        }

        $user->birthdate    = $request->birthdate;
        $user->gender       = $request->gender;
        $user->first_name   = $request->first_name;
        $user->last_name    = $request->last_name;
        $user->address      = $request->address;
        $user->phone        = $request->phone;

        try {
            $user->save();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot update user, fail'], 500);
        }
        LogActivity::addToLog($request, 'Update profile');
        return response()->json(['status' => 'success', 'code' => 200, 'data' => $user], 200);
    }

    public function tes()
    {
        return Carbon::now()->addDays(3)->format('d F Y');
    }

}
    