<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Travel;
use Illuminate\Support\Facades\Auth;
use App\Helpers\LogActivity;

class TravelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function get(Request $request)
    {
        try {
            $travels    = Travel::all();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Failed to fetch data travel', 'error' => $e->getMessage(), 'code' => 500], 500);
        }
        
        LogActivity::addToLog($request, 'Get all travels');
        
        return response()->json(['status' => 'success', 'data' => $travels, 'code' => 200], 200);
    }
    
    public function show(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }

        try {
            $travel = Travel::findOrFail($id);
        } catch (\Exception $e)
        {
            return response()->json(['status' => 'error', 'message' => 'Failed to fetch data travel, not found', 'error' => $e->getMessage(), 'code' => 404], 404);
        }

        LogActivity::addToLog($request, 'Show a travel');
        
        return response()->json(['status' => 'success', 'data' => $travel, 'code' => 200], 200);
    }
    
    public function delete(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'ID not found',
                'code' => 500
                ], 500);
        }
        
        try {
            $travel = Travel::findOrFail($id);
        } catch (\Exception $e)
        {
            return response()->json(['status' => 'error', 'message' => 'Failed to fetch data travel, not found', 'error' => $e->getMessage(), 'code' => 404], 404);
        }

        try {
            $travel->delete();
        } catch (\Exception $e)
        {
            return response()->json(['status' => 'error', 'message' => 'Failed to delete data travel', 'error' => $e->getMessage(), 'code' => 500], 500);
        }

        LogActivity::addToLog($request, 'Delete a travel');
        
        return response()->json(['status' => 'success', 'data' => $travel, 'code' => 200], 200);
    }
    
    public function update(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'id_travel'     => 'required',
            'travel_name'   => 'required',
            'email'         => 'required',
            'address'       => 'required',
            'phone'         => 'required|min:11|max:13',
            'social_media_type_1'       => 'required',
            'social_media_type_2'       => 'required',
            'social_media_1'       => 'required',
            'social_media_2'       => 'required',
            'website'       => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }
        
        try {
            $travel = Travel::findOrFail(decrypt($request->id_travel));
        } catch (\Exception $e)
        {
            return response()->json(['status' => 'error', 'message' => 'Failed to fetch data travel, not found', 'error' => $e->getMessage(), 'code' => 404], 404);
        }

        if ($request->has('logo'))
        {
            $old_logo           = decrypt($travel->logo);
            $logo               = $request->file('logo');
            $name               = $logo->getClientOriginalName();
            $ext                = $logo->getClientOriginalExtension();
            $logo_name          = 'TL-' . rand(000,999) . '-' . date('dmY') . '.' . $ext;
            $destination_path   = storage_path('/app/public/travel_logo');

            if ($request->file('logo')->move($destination_path, $logo_name)) 
            {
                $travel->logo   = $logo_name;
            }
            else 
            {
                return response()->json(['status' => 'error', 'code' => 500, 'message' => 'Cannot update logo, fail', 'error' => $e->getMessage()], 500);
            }
        }

        $travel->travel_name    = $request->travel_name;
        $travel->email          = $request->email;
        $travel->address        = $request->address;
        $travel->phone          = $request->phone;
        $travel->social_media_type_1    = $request->social_media_type_1;
        $travel->social_media_type_2    = $request->social_media_type_2;
        $travel->social_media_1         = $request->social_media_1;
        $travel->social_media_2         = $request->social_media_2;
        $travel->website                = $request->website;
        
        try {
            $travel->save();
        } catch (\Exception $e)
        {
            return response()->json(['status' => 'error', 'message' => 'Failed to update data travel', 'error' => $e->getMessage(), 'code' => 500], 500);
        }

        LogActivity::addToLog($request, 'Update data travel');
        
        return response()->json(['status' => 'success', 'data' => $travel, 'code' => 200], 200);
    }

    //
}
