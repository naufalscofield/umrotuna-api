<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use App\Models\Voucher;
use Illuminate\Support\Facades\Validator;

class VoucherController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'id_travel'     => 'required',
            'code'          => 'required',
            'description'   => 'required',
            'type'          => 'required',
            'discount'      => 'required|numeric',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        try {
            $id_travel  = decrypt($request->id_travel);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'ID travel not found',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        $voucher = new Voucher;
        if ($request->has('id_travel'))
        {
            $voucher->id_travel     = $id_travel;
        }
        $voucher->code          = $request->code;
        $voucher->description   = $request->description;
        $voucher->type          = $request->type;
        $voucher->discount      = $request->discount;
        
        try {
            $voucher->save();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to save data voucher',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Adding voucher');

        return response()->json([
            'message' => 'Voucher saved',
            'status' => 'success',
            'code' => 200,
            'data'  => $voucher
            ], 200);
    }

    public function get(Request $request)
    {
        try {
            $voucher = Voucher::all();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data voucher',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Get voucher');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $voucher
            ], 200);
    }

    public function show(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id vouche',
                'code' => 500
                ], 500);
        }

        try {
            $voucher = Voucher::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data voucher, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        LogActivity::addToLog($request, 'Show voucher by id');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $voucher
            ], 200);
    }

    public function showByIdTravel(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id travel',
                'code' => 500
                ], 500);
        }

        try {
            $voucher = Voucher::where('id_travel', $id)->get();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data voucher',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Show voucher by id travel');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'data'  => $voucher
            ], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_voucher'     => 'required',
            'code'           => 'required',
            'description'    => 'required',
            'type'           => 'required',
            'discount'       => 'required',
        ]);

        if ($validator->fails())
        {
            $error_messages  = $validator->messages()->get('*');

            return response()->json(['status' => 'error', 'code' => 400, 'message' => $validator->errors()->first()], 400);
        }

        try {
            $request->id_voucher = decrypt($request->id_voucher);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Decryption issue on id voucher',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        try {
            $voucher = Voucher::findOrFail($request->id_voucher);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data voucher, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        $voucher->code           = $request->code;
        $voucher->description    = $request->description;
        $voucher->type           = $request->type;
        $voucher->discount       = $request->discount;
        
        try {
            $voucher->save();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to update data voucher',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Update voucher');

        return response()->json([
            'status' => 'success',
            'message' => 'Voucher updated',
            'code' => 200,
            'data'  => $voucher
            ], 200);
    }

    public function delete(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'status' => 'error',
                'message' => 'Decryption issue on id voucher',
                'code' => 500
                ], 500);
        }

        try {
            $voucher = Voucher::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to fetch data voucher, data not found',
                'error' => $e->getMessage(),
                'code' => 404,
                ], 404);
        }

        try {
            $voucher->delete();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to delete data voucher',
                'error' => $e->getMessage(),
                'code' => 500,
                ], 500);
        }

        LogActivity::addToLog($request, 'Delete voucher');

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'message'  => 'Voucher deleted'
            ], 200);
    }
}
