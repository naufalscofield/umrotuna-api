<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class LogActivity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject', 'url', 'method', 'ip', 'agent', 'user_id'
    ];

    // protected $hidden   = 'id';
    protected $appends  = array('enc_id');

    public function getEncIdAttribute()
    {
        return encrypt($this->attributes['id']);  
    }
}