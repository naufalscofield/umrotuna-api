<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;
use App\Models\Schedule;
use App\Models\Payment;
use App\Models\Bookingdetail;

class Booking extends Model 
{
    protected $table = 'booking';
    use SoftDeletes;

    // protected $hidden   = 'id';
    protected $appends  = array('enc_id');

    public function getEncIdAttribute()
    {
        return encrypt($this->attributes['id']);  
    }

    public function persons()
    {
        return $this->hasMany(Bookingdetail::class, 'id_booking', 'id');
    }

    public function schedule()
    {
        return $this->hasOne(Schedule::class, 'id', 'id_schedule');
    }

    public function payment()
    {
        return $this->hasMany(Payment::class, 'id_booking', 'id');
    }
}
