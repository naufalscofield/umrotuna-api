<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;
use App\Models\Mahram;
use App\Models\Travel;

class Mahramfee extends Model 
{
    use SoftDeletes;
    protected $table = 'mahram_fee';
    // protected $hidden   = 'id';
    protected $appends  = array('enc_id');

    public function getEncIdAttribute()
    {
        return encrypt($this->attributes['id']);  
    }

    public function mahram()
    {
        return $this->hasOne(Mahram::class, 'id', 'id_mahram');
    }

    public function travel()
    {
        return $this->hasOne(Travel::class, 'id', 'id_travel');
    }
}
