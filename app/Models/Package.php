<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Travel;
use App\Models\Schedule;
use App\Models\Triptype;
use Illuminate\Support\Facades\Crypt;

class Package extends Model 
{
    use SoftDeletes;
    protected $table = 'packages';

    // protected $hidden   = 'id';
    protected $appends  = array('enc_id');

    public function getEncIdAttribute()
    {
        return encrypt($this->attributes['id']);  
    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class, 'id_package', 'id');
    }

    public function travel()
    {
        return $this->hasOne(Travel::class, 'id', 'id_travel');
    }

    public function triptype()
    {
        return $this->hasOne(Triptype::class, 'id', 'id_type');
    }

    public function getFlyerAttribute($value)
    {
        return encrypt($this->attributes['flyer']);
    }

    // public function getIdAttribute()
    // {
    //     return encrypt($this->attributes['id']);
    // }
}
