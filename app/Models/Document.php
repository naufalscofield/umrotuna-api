<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;

class Document extends Model 
{
    use SoftDeletes;
    protected $table = 'document';

    // protected $hidden   = 'id';
    protected $appends  = array('enc_id');

    public function getEncIdAttribute()
    {
        return encrypt($this->attributes['id']);  
    }

    public function getKtpAttribute($value)
    {
        return encrypt($this->attributes['ktp']);
    }
    
    public function getKkAttribute($value)
    {
        return encrypt($this->attributes['kk']);
    }
    
    public function getPassportAttribute($value)
    {
        return encrypt($this->attributes['passport']);
    }
    
    public function getMarriageBookAttribute($value)
    {
        return encrypt($this->attributes['marriage_book']);
    }
    
    public function getChildBirthCertificateAttribute($value)
    {
        return encrypt($this->attributes['child_birth_certificate']);
    }
    
    public function getMeningitisVaccineAttribute($value)
    {
        return encrypt($this->attributes['meningitis_vaccine']);
    }
    
    public function getPhotoAttribute($value)
    {
        return encrypt($this->attributes['photo']);
    }
    
    // public function getIdAttribute()
    // {
    //     return encrypt($this->attributes['id']);
    // }
}
