<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;
use App\Models\Document;
use App\Models\Personstatus;

class Bookingdetail extends Model 
{
    protected $table = 'booking_detail';
    use SoftDeletes;

    // protected $hidden   = 'id';
    protected $appends  = array('enc_id');

    public function getEncIdAttribute()
    {
        return encrypt($this->attributes['id']);  
    }

    public function document()
    {
        return $this->hasOne(Document::class, 'id_booking_detail', 'id');
    }

    public function status()
    {
        return $this->hasOne(Personstatus::class, 'id', 'status');
    }
}
