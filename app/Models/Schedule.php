<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Package;
use Illuminate\Support\Facades\Crypt;

class Schedule extends Model 
{
    use SoftDeletes;
    protected $table = 'schedule';

    // protected $hidden   = 'id';
    protected $appends  = array('enc_id');

    public function getEncIdAttribute()
    {
        return encrypt($this->attributes['id']);  
    }

    public function package()
    {
        return $this->hasOne(Package::class, 'id', 'id_package');
    }

    public function getFlyerAttribute($value)
    {
        return encrypt($this->attributes['flyer']);
    }
}
