<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;

class Triptype extends Model 
{
    use SoftDeletes;
    protected $table = 'trip_type';

    // protected $hidden   = 'id';
    protected $appends  = array('enc_id');

    public function getEncIdAttribute()
    {
        return encrypt($this->attributes['id']);  
    }
}
