<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->id();
            $table->integer('id_booking');
            $table->string('type');
            $table->integer('total_payment');
            $table->string('payment_method')->nullable();
            $table->timestamp('pay_date')->nullable();
            $table->integer('id_voucher')->nullable();
            $table->integer('total_discount')->nullable();
            $table->date('expired');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
}
