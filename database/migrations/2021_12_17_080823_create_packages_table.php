<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->integer('id_travel');
            $table->string('package_name');
            $table->integer('id_type');
            $table->integer('price');
            $table->integer('dp');
            $table->string('currency');
            $table->longText('description');
            $table->longText('flyer')->nullable();
            $table->integer('active');
            $table->integer('refundable');
            $table->integer('reschedule');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
