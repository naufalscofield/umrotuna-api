<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travels', function (Blueprint $table) {
            $table->id();
            $table->string('travel_name');
            $table->string('email');
            $table->longText('address');
            $table->string('phone');    
            $table->longText('logo');    
            $table->string('social_media_type_1')->nullable();
            $table->string('social_media_1')->nullable();
            $table->string('social_media_type_2')->nullable();
            $table->string('social_media_2')->nullable();
            $table->string('website')->nullable();
            $table->integer('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travels');
    }
}
